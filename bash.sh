pip install mmcv-full==1.4.5 -f https://download.openmmlab.com/mmcv/dist/cu111/torch1.9.0/index.html

python demo/image_demo.py demo/demo.jpg \
    configs/faster_rcnn/faster_rcnn_r50_fpn_1x_coco.py \
    checkpoints/faster_rcnn_r50_fpn_1x_coco_20200130-047c8118.pth \
    --device cuda:0

python tools/test.py \
    configs/faster_rcnn/faster_rcnn_r50_fpn_1x_coco.py \
    checkpoints/faster_rcnn_r50_fpn_1x_coco_20200130-047c8118.pth \
    --show-dir faster_rcnn_r50_fpn_1x_results


Train__________________________________
faster_rcnn
bash ./tools/dist_train.sh \
    configs/faster_rcnn/faster_rcnn_r50_fpn_1x_coco.py \
    8 

atss
bash ./tools/dist_train.sh \
    configs/atss/atss_r50_fpn_1x_coco.py \
    8

dyhead_atss
bash ./tools/dist_train.sh \
    configs/dyhead/atss_r50_fpn_dyhead_1x_coco.py \
    8 

bash ./tools/dist_train.sh \
    train_configs/atss_r101_fpn_dyhead_1x_coco.py \
    8 

bash ./tools/dist_train.sh \
    train_configs/atss_swin-t-p4-w7_fpn_dyhead_1x_coco.py \
    8
bash ./tools/dist_train.sh \
    train_configs/atss_swin-t-p4-w7_fpn_dyhead_fp16_ms-crop_1x_coco.py \
    8
bash ./tools/dist_train.sh \
    train_configs/atss_swin-s-p4-w7_fpn_dyhead_fp16_ms-crop_1x_coco.py \
    8
bash ./tools/dist_train.sh \
    train_configs/atss_swin-b-p4-w7_fpn_dyhead_fp16_ms-crop_1x_coco.py \
    8
bash ./tools/dist_train.sh \
    train_configs/atss_swin-b-p4-w12_fpn_dyhead_fp16_ms-crop_1x_coco.py \
    8
bash ./tools/dist_train.sh \
    train_configs/atss_swin-l-p4-w7_fpn_dyhead_fp16_ms-crop_1x_coco.py \
    8
bash ./tools/dist_train.sh \
    train_configs/atss_swin-l-p4-w12_fpn_dyhead_fp16_ms-crop_1x_coco.py \
    8




swin
bash ./tools/dist_train.sh \
    configs/swin/mask_rcnn_swin-t-p4-w7_fpn_1x_coco.py \
    8
bash ./tools/dist_train.sh \
    configs/swin/mask_rcnn_swin-s-p4-w7_fpn_fp16_ms-crop-3x_coco.py \
    8

bash ./tools/dist_train.sh \
    configs/swin/retinanet_swin-t-p4-w7_fpn_1x_coco.py \
    8
   




bash ./tools/dist_train.sh \
    train_configs/atss_regnetx-1.6GF_fpn_dyhead_1x_coco.py \
    8

